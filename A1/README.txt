Data Mining/Machine Learning
Fall 2018

Team Members:
- Andrew Leonov
- Timothy Abramov
- Benjamin Weisman

Unfortunately we started working in a different repository,
so our progress can be tracked in the repo located at the link below.

Git Repository: https://github.com/TimSAS/Data-Mining

==============================
How to run Apriori Code
==============================
- download the code from the repository listed above
- sudo apt-get install g++
- browse to the /src folder of the project
- compile the code with 
	g++ -std=c++11 AprioriImplementation.cpp
- run the compiled executable with
	./a.out